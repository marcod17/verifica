<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Support\Facades\Validator;

class MoviesController extends Controller
{
    public function store(Request $request) {

        $validator = validator::make($request->all(),[
            'title' => 'required|max:255',
            'duration' => 'required|integer|max:32767',
            'director_id' => 'required|integer|exists:directors,id'
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        };

        $movie = new Movie();
        $movie->title = $request->input('title');
        $movie->duration = $request->input('duration');
        $movie->director_id = $request->input('director_id');

        try {
            $movie->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return $movie;
    }
    public function getAll(Request $request) {
        $movies = Movie::with('director')->get();
        return $movies;
    }
    public function get(Request $request, $id) {
        $movie = Movie::findOrFail($id);
        return $movie;
    }
    public function update(Request $request, $id) {
        $validator = validator::make($request->all(),[
            'title' => 'required|max:255',
            'duration' => 'required|integer|max:32767',
            'director_id' => 'required|integer|exists:authors,id'
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        };

        $movie = Movie::findOrFail($id);
        $movie->title = $request->input('title');
        $movie->duration = $request->input('duration');
        $movie->director_id = $request->input('director_id');

        try {
            $movie->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return $movie;
    }
    public function delete(Request $request, $id) {
        $movie = Movie::findOrFail($id);
        $movie->delete();
        return response()->json(null, 204);
    }
}
