<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Routes for Movies
 */
Route::post('/movies', 'MoviesController@store');
Route::get('/movies', 'MoviesController@getAll');
Route::get('/movies/{id}', 'MoviesController@get');
Route::put('/movies/{id}', 'MoviesController@update');
Route::delete('/movies/{id}', 'MoviesController@delete');
/**
 * Routes for Directors
 */
Route::post('/directors', 'DirectorsController@store');
Route::get('/directors', 'DirectorsController@getAll');
Route::get('/directors/{id}', 'DirectorsController@get');
Route::put('/directors/{id}', 'DirectorsController@update');
Route::delete('/directors/{id}', 'DirectorsController@delete');
// display all movies by specific director
Route::get('/directors/{id}/movies', 'DirectorsController@getDirectorsMovies');
