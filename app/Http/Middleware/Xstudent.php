<?php

namespace App\Http\Middleware;

use Closure;

class Xstudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)->header('X-student-name', 'Marco Del Bianco');
    }
}
