<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Director;
use Illuminate\Support\Facades\Validator;

class DirectorsController extends Controller
{
    public function store(Request $request) {
        $validator = validator::make($request->all(),[
            'firstname' => 'required|max:150',
            'lastname' => 'required|max:150',
            'oscar_number' => 'required|integer'
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        };

        $director = new Director();
        $director->firstname = $request->input('firstname');
        $director->lastname = $request->input('lastname');
        $director->oscar_number = $request->input('oscar_number');

        try {
            $director->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return $director;
    }
    public function getAll(Request $request) {
        $director = Director::get();
        return $director;
    }
    public function get(Request $request, $id) {
        $director = Director::with('movies')->findOrFail($id);
        return $director;
    }
    public function update(Request $request, $id) {
        $validator = validator::make($request->all(),[
            'firstname' => 'required|max:150',
            'lastname' => 'required|max:150',
            'oscar_number' => 'required|integer'
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        };

        $director = Director::findOrFail($id);
        $director->firstname = $request->input('firstname');
        $director->lastname = $request->input('lastname');
        $director->oscar_number = $request->input('oscar_number');

        try {
            $director->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return $director;
    }
    public function delete(Request $request, $id) {
        $director = Director::findOrFail($id);
        $director->delete();
        return response()->json(null, 204);
    }
    public function getDirectorsMovies(Request $request, $id) {
        $director = Director::findOrFail($id);
        return $director->movies;
    }
    
}
